import XMonad
import System.IO
import System.Exit
import XMonad.Util.Run(spawnPipe)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Layout.ResizableTile
import XMonad.Layout.NoBorders
import qualified XMonad.StackSet as W
import qualified Data.Map as M

import XMonad.Layout.DragPane

import XMonad.Layout.AutoMaster
import XMonad.Layout.Column
import XMonad.Layout.Grid
import XMonad.Layout.Circle



main = do
    xmproc <- spawnPipe "xmobar --screen=0 ~/.xmonad/xmobar"
    xmonad defaultConfig
        { modMask = mod4Mask
        , startupHook = myStartupHook
        , layoutHook = avoidStruts $ smartBorders $  myLayout
        , logHook = dynamicLogWithPP $ xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppHidden = xmobarColor "white" ""
                        , ppHiddenNoWindows = xmobarColor "#636363" ""
                        , ppCurrent = xmobarColor "#CFF547" ""
                        , ppUrgent = xmobarColor "#09cdda" ""
                        , ppTitle = xmobarColor "#CFF547" "" . shorten 50
                        }
        , normalBorderColor  = "#222222"
        , focusedBorderColor = "#dddddd"
        , keys = myKeys
    }

myStartupHook = do
    spawn "gnome-settings-daemon"
    spawn "xcompmgr"
    spawn "naturalscrolling"
    spawn "nitrogen --restore"
    spawn "xsetroot -cursor_name left_ptr"
    return ()

myLayout = tiled ||| Mirror tiled ||| noBorders Full
  where
    tiled   = ResizableTall nmaster delta ratio sratio
    nmaster = 1 -- The default number of windows in the master pane
    ratio   = 0.6 -- Default proportion of screen occupied by master pane
    delta   = 3/100 -- Percent of screen to increment by when resizing panes
    sratio  = [0.7]

keysToAdd x = [
        ((modMask x, xK_d), spawn "dmenu_run -b -nb black -nf white -sb black -sf \"#CFF547\"")
        , ((modMask x .|. shiftMask, xK_d), spawn "gmrun")
        , ((modMask x, xK_Escape), io (exitWith ExitSuccess))
        , ((modMask x, xK_b), sendMessage ToggleStruts)
        , ((modMask x, xK_n), sendMessage MirrorShrink)
        , ((modMask x, xK_i), sendMessage MirrorExpand)
    ]
    ++
    [((m .|. modMask x, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_q, xK_w, xK_e] [1,0,2]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

keysToRemove x = [
        (modMask x, xK_q) -- Use q for window management instead
        , (modMask x, xK_n)
        , (modMask x, xK_w) -- Remove default xinerama shortcuts
        , (modMask x, xK_e)
        , (modMask x, xK_r)
        , (modMask x, xK_p)
        , (modMask x .|. shiftMask, xK_w)
        , (modMask x .|. shiftMask, xK_e)
        , (modMask x .|. shiftMask, xK_r)
        , (modMask x .|. shiftMask, xK_q)
    ]

strippedKeys x = foldr M.delete (keys defaultConfig x) (keysToRemove x)

myKeys x = M.union (strippedKeys x) (M.fromList (keysToAdd x))
